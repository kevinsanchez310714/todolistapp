import 'package:cloud_firestore/cloud_firestore.dart';

class UserModel {
  String id;
  String displayName;
  String photoURL;
  String email;

  UserModel({
    required this.id,
    required this.displayName,
    required this.photoURL,
    required this.email
  });

  factory UserModel.fromFirestore(DocumentSnapshot userDoc) {
    Map<String, dynamic>? userData = userDoc.data() as Map<String, dynamic>?;
    return UserModel(
      id: userDoc.id,
      displayName: userData?['displayName'] ?? '',
      photoURL: userData?['photoURL'] ?? '',
      email: userData?['email'] ?? '',
    );
  }

  void setFromFireStore(DocumentSnapshot userDoc) {
    Map<String, dynamic>? userData = userDoc.data() as Map<String, dynamic>?;
    id = userDoc.id;
    displayName = userData?['displayName'] ?? '';
    photoURL = userData?['photoURL'] ?? '';
    email = userData?['email'] ?? '';
  }

}