class TasksModel {
  String id;
  final String title;
  final String description;
  final String status;
  final String date;

  TasksModel({
    this.id = '',
    required this.title,
    required this.description,
    required this.status,
    required this.date
  });

  Map<String, dynamic> toJson() => {
    'id': id,
    'title': title,
    'description': description,
    'status': status,
    'date': date
  };

  static TasksModel fromJson(Map<String, dynamic> json) => TasksModel(
    id: json['id'],
    title: json['title'],
    description: json['description'],
    status: json['status'],
    date: json['date']
  );
}