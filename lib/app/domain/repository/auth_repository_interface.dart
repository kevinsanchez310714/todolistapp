import 'package:firebase_auth/firebase_auth.dart';
import 'package:todolist/app/domain/model/user_model.dart';
import 'package:todolist/app/domain/response/sign_in_response.dart';

abstract class AuthRepositoryInterface {
  Future<UserModel> saveUser();
  Future<User?> get user;
  Future<void> singOut();
  Future<SignInResponse> signInWithEmailAndPassword(String email, String password);
}