import 'package:todolist/app/domain/request/sing_up_data_request.dart';
import 'package:todolist/app/domain/response/sing_up_response.dart';

abstract class SingUpRepositoryInterface {
  Future<SingUpResponse> register(SingUpDataRequest data);
}


