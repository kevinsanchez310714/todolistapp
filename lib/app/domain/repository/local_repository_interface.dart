import 'package:todolist/app/domain/model/user_model.dart';

abstract class LocalRepositoryInterface {
  Future<UserModel> getUser();
}