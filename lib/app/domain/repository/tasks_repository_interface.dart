import 'package:todolist/app/domain/model/tasks_model.dart';
import 'package:todolist/app/domain/request/tasks_data_request.dart';

abstract class TasksRepositoryInterface {
  Future createTask(TaskDataRequest data);
  Future<List<TasksModel>> listTasks();
  Future changeStatus(String taskId, String status);
  Future deleteTask(String taskId);
}