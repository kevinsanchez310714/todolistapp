class TaskDataRequest{
  final String title;
  final String description;
  final String status;
  final String date;

  TaskDataRequest({required this.title, required this.description, required this.status, required this.date});

}