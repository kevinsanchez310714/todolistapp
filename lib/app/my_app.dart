import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist/app/data/services/auth_repository_services.dart';
import 'package:todolist/app/data/services/local_repository_services.dart';
import 'package:todolist/app/data/services/sing_up_repository_services.dart';
import 'package:todolist/app/data/services/tasks_repository_services.dart';
import 'package:todolist/app/domain/repository/auth_repository_interface.dart';
import 'package:todolist/app/domain/repository/local_repository_interface.dart';
import 'package:todolist/app/domain/repository/sing_up_repository_interface.dart';
import 'package:todolist/app/domain/repository/tasks_repository_interface.dart';
import 'package:todolist/app/my_app_controller.dart';
import 'package:todolist/app/presentation/global_controller/session_controller.dart';
import 'package:todolist/app/presentation/routes/app_routes.dart';
import 'package:todolist/app/presentation/routes/routes.dart';

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
        Provider<LocalRepositoryInterface>(
          create: (_) => LocalRepositoryServices(),
        ),
        Provider<AuthRepositoryInterface>(
          create: (_) => AuthRepositoryServices(FirebaseAuth.instance),
        ),
        Provider<SingUpRepositoryInterface>(
          create: (_) => SingUpRepositoryServices(FirebaseAuth.instance),
        ),
      Provider<TasksRepositoryInterface>(
          create: (_) => TasksRepositoryServices(FirebaseFirestore.instance),
        ),
        ChangeNotifierProvider(
          create: (context) {
            return SessionController(authRepositoryInterface: context.read<AuthRepositoryInterface>());
          },
        ),
        ChangeNotifierProvider(
          create: (context) {
            return MyAppController(
              localRepositoryInterface: context.read<LocalRepositoryInterface>(),
              authRepositoryInterface: context.read<AuthRepositoryInterface>()
            );
          },
        ),
      ],
      child: Builder(
        builder: (newContext){
          return Consumer<MyAppController>(
            builder: (context, value, child){
              return MaterialApp(
                debugShowCheckedModeBanner: false,
                title: 'ToDo List',
                theme: ThemeData(
                  colorScheme: ColorScheme.fromSeed(seedColor: const Color(0xFF16af97)),
                ),
                initialRoute: Routes.SPLASH,
                routes: appRoutes
              );
            },
          );
        },
      ),
    );
  }

}