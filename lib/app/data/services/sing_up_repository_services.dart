import 'package:firebase_auth/firebase_auth.dart';
import 'package:todolist/app/domain/repository/sing_up_repository_interface.dart';
import 'package:todolist/app/domain/request/sing_up_data_request.dart';
import 'package:todolist/app/domain/response/sing_up_response.dart';

class SingUpRepositoryServices implements SingUpRepositoryInterface{

  final FirebaseAuth _auth;

  SingUpRepositoryServices(this._auth);

  @override
  Future<SingUpResponse> register(SingUpDataRequest data) async {
    try {
      final userCredentials = await _auth.createUserWithEmailAndPassword(
          email: data.email,
          password: data.password
      );
      await userCredentials.user!.updateDisplayName(
        "${data.name} ${data.lastname}"
      );
      return SingUpResponse(null, userCredentials.user);
    } on FirebaseAuthException catch (e) {
      return SingUpResponse(parseStringToSingUpError(e.code), null);
    }
  }

}