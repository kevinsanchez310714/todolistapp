import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:todolist/app/domain/model/tasks_model.dart';
import 'package:todolist/app/domain/repository/tasks_repository_interface.dart';
import 'package:todolist/app/domain/request/tasks_data_request.dart';


class TasksRepositoryServices implements TasksRepositoryInterface{

  final FirebaseFirestore _firestore;

  TasksRepositoryServices(this._firestore);

  @override
  Future createTask(TaskDataRequest data) async {
    final docTask = await _firestore.collection('tasks').doc();

    final task = TasksModel(
      id: docTask.id,
      title: data.title,
      description: data.description,
      status: data.status,
      date: data.date
    );
    final json = task.toJson();

    await docTask.set(json);
  }

  @override
  Future<List<TasksModel>> listTasks() async {
    final querySnapshot = await _firestore.collection('tasks').get();

    final tasksList = querySnapshot.docs
        .map((doc) => TasksModel.fromJson(doc.data()))
        .toList();

    tasksList.sort((a, b) {
      int statusComparison = a.status.compareTo(b.status);
      if (statusComparison != 0) {
        return statusComparison;
      }
      return b.date.compareTo(a.date);
    });

    return tasksList;
  }

  @override
  Future changeStatus(String taskId,String status) async {
    final docTask = _firestore.collection('tasks').doc(taskId);
    final response = await docTask.update({
      'status': status
    });
  }

  @override
  Future deleteTask(String taskId) async {
    final docTask = _firestore.collection('tasks').doc(taskId);
    docTask.delete();
  }

}