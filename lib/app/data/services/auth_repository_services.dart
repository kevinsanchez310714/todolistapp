import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:todolist/app/domain/model/user_model.dart';
import 'package:todolist/app/domain/repository/auth_repository_interface.dart';
import 'package:todolist/app/domain/response/sign_in_response.dart';

class AuthRepositoryServices extends AuthRepositoryInterface {

  final FirebaseAuth _auth;
  User? _user;

  final Completer<void> _completer = Completer();

  AuthRepositoryServices(this._auth){
    _init();
  }

  @override
  Future<UserModel> saveUser() {
    // TODO: implement saveUser
    throw UnimplementedError();
  }

  @override
  // TODO: implement user
  Future<User?> get user async {
    await _completer.future;
    return _user;
  }

  void _init() async {
    _auth.authStateChanges().listen((User? user) {
      if(!_completer.isCompleted){
        _completer.complete();
      }
      _user = user;
    });
  }

  @override
  Future<void> singOut() {
    return _auth.signOut();
  }

  @override
  Future<SignInResponse> signInWithEmailAndPassword(String email, String password) async {
    try{
      final userCredential = await _auth.signInWithEmailAndPassword(email: email, password: password);
      final user = userCredential.user!;
      return SignInResponse(null, user);
    } on FirebaseAuthException catch (e){
      return SignInResponse(parseStringToSignInError(e.code), null);
    }
  }
}