import 'package:flutter/widgets.dart' show Widget, BuildContext;
import 'package:todolist/app/presentation/screens/auth/auth_screen.dart';
import 'package:todolist/app/presentation/screens/home/home_screen.dart';
import 'package:todolist/app/presentation/screens/sing_up/sing_up_screen.dart';
import 'package:todolist/app/presentation/screens/splash/splash_screen.dart';
import 'package:todolist/app/presentation/screens/tasks/tasks_screen.dart';
import 'routes.dart';

Map<String, Widget Function(BuildContext)> get appRoutes => {
  Routes.SPLASH: (context) => SplashScreen.init(context),
  Routes.SINGUP: (context) => SingUpScreen.init(context),
  Routes.LOGIN: (context) => AuthScreen.init(context),
  Routes.HOME: (context) => HomeScreen.init(context),
  Routes.TASKS: (context) => TasksScreen.init(context),
};