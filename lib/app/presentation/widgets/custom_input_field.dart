import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomInputField extends StatefulWidget{

  final void Function(String)? onChanged;
  final String label;
  final TextInputType? inputType;
  final bool isPassword;
  final String? Function(String?)? validator;
  final TextEditingController? controller;
  final bool isLargeText;
  final void Function()? onTap;
  final IconData? icon;

  const CustomInputField({
    super.key,
    this.onChanged,
    required this.label,
    this.inputType,
    this.isPassword = false,
    this.validator,
    this.controller,
    this.isLargeText = false,
    this.onTap,
    this.icon
  });

  @override
  State<CustomInputField> createState() => _CustomInputFieldState();
}

class _CustomInputFieldState extends State<CustomInputField> {

  late bool _obscureText;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _obscureText = widget.isPassword;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FormField<String>(
      initialValue: '',
      validator: widget.validator,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      builder: ((state){
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              controller: widget.controller,
              obscureText: _obscureText,
              keyboardType: widget.inputType,
              maxLines: widget.isLargeText ? null : 1,
              onTap: widget.onTap,
              onChanged: (text){
                if(widget.validator != null){
                  state.setValue(text);
                  state.validate();
                }
                if(widget.onChanged != null){
                  widget.onChanged!(text);
                }
              },
              decoration: InputDecoration(
                labelText: widget.label,
                border: const OutlineInputBorder(),
                suffixIcon: widget.isPassword ? CupertinoButton(
                  onPressed: () {
                    _obscureText = !_obscureText;
                    setState(() { });
                  },
                  child: Icon(_obscureText ? Icons.visibility : Icons.visibility_off),
                ) : widget.icon != null ? CupertinoButton(
                  onPressed: () { },
                  child: Icon(widget.icon),
                ) : null
              ),
            ),
            if(state.hasError)
              Text(
                state.errorText!,
                style: const TextStyle(
                  color: Colors.redAccent,
                ),
              )
          ],
        );
      }),
    );
  }
}