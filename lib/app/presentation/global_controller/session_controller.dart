import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:todolist/app/domain/repository/auth_repository_interface.dart';

class SessionController with ChangeNotifier {

  final AuthRepositoryInterface authRepositoryInterface;

  SessionController({required this.authRepositoryInterface});

  User? _user;
  User? get user => _user;

  void setUser(User user){
    _user = user;
    notifyListeners();
  }

  Future<void> singOut() async {
    await authRepositoryInterface.singOut();
    _user = null;
    notifyListeners();
  }

}