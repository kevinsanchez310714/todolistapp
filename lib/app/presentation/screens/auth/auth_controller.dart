import 'package:flutter/cupertino.dart';
import 'package:todolist/app/domain/repository/auth_repository_interface.dart';
import 'package:todolist/app/domain/repository/local_repository_interface.dart';
import 'package:todolist/app/domain/response/sign_in_response.dart';
import 'package:todolist/app/presentation/global_controller/session_controller.dart';
import 'package:todolist/app/presentation/routes/routes.dart';
import 'package:todolist/app/presentation/widgets/dialogs.dart';
import 'package:todolist/app/presentation/widgets/progress_dialog.dart';

class AuthController with ChangeNotifier {

  final SessionController _sessionController;

  final LocalRepositoryInterface localRepositoryInterface;
  final AuthRepositoryInterface authRepositoryInterface;

  AuthController(this._sessionController, { required this.localRepositoryInterface, required this.authRepositoryInterface});

  final GlobalKey<FormState> formKey = GlobalKey();

  TextEditingController correo = TextEditingController();
  TextEditingController password = TextEditingController();

  Future<void> sendLoginForm(BuildContext context) async {
    if(formKey.currentState!.validate()){
      ProgressDialog.show(context);
      final response = await submit();
      Navigator.pop(context);
      if(response.error != null){
        late String content;
        switch(response.error){
          case SignInError.networkRequestFailed:
            content = "No hay acceso a la red";
            break;
          case SignInError.userDisabled:
            content = "El usuario se encuentra deshabilitado";
            break;
          case SignInError.userNotFound:
            content = "El usuario no existe";
            break;
          case SignInError.wrongPassword:
            content = "Contraseña incorrecta";
            break;
          case SignInError.invalidCredential:
            content = "Credenciales invalidas";
            break;
          case SignInError.tooManyRequests:
            content = "Usuario bloqueado";
            break;
          case SignInError.unknown:
            content = "Ha ocurrido un error";
            break;
          default:
            content = "Ha ocurrido un error";
            break;
        }
        Dialogs.alert(context, title: 'ERROR', content: content);
      }else{
        Navigator.pushReplacementNamed(context, Routes.HOME);
      }
    }
  }

  Future<SignInResponse> submit() async {
    final response = await authRepositoryInterface.signInWithEmailAndPassword(correo.text, password.text);
    if(response.error ==  null){
      _sessionController.setUser(response.user!);
    }
    return response;
  }


}