import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist/app/domain/repository/auth_repository_interface.dart';
import 'package:todolist/app/domain/repository/local_repository_interface.dart';
import 'package:todolist/app/presentation/global_controller/session_controller.dart';
import 'package:todolist/app/presentation/routes/routes.dart';
import 'package:todolist/app/presentation/screens/auth/auth_controller.dart';
import 'package:todolist/app/presentation/widgets/custom_input_field.dart';
import 'package:todolist/app/utils/input_validator.dart';

class AuthScreen extends StatefulWidget {
  const AuthScreen._();

  static Widget init(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => AuthController(context.read<SessionController>(),
          localRepositoryInterface: context.read<LocalRepositoryInterface>(),
          authRepositoryInterface: context.read<AuthRepositoryInterface>()),
      builder: (_, __) => const AuthScreen._(),
    );
  }

  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  @override
  Widget build(BuildContext context) {
    final controller = Provider.of<AuthController>(context);
    return Scaffold(
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Container(
            width: double.infinity,
            height: double.infinity,
            color: Colors.transparent,
            padding: const EdgeInsets.all(15.0),
            child: Form(
              key: controller.formKey,
              child: ListView(
                children: [
                  const SizedBox(
                    height: 100,
                  ),
                  Image.asset(
                    "assets/logo.png",
                    width: 250,
                    height: 200,
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  CustomInputField(
                    controller: controller.correo,
                    label: "Correo",
                    onChanged: (text) {},
                    inputType: TextInputType.emailAddress,
                    validator: (text) {
                      return isValidEmail(text!) ? null : "Correo invalido";
                    },
                  ),
                  const SizedBox(
                    height: 15.0,
                  ),
                  CustomInputField(
                    controller: controller.password,
                    label: "Contraseña",
                    onChanged: (text) {},
                    isPassword: true,
                    validator: (text) {
                      if (text!.trim().length >= 6) {
                        return null;
                      }
                      return "Contraseña invalida";
                    },
                  ),
                  const SizedBox(
                    height: 30.0,
                  ),
                  ElevatedButton(
                      onPressed: () => controller.sendLoginForm(context),
                      style: ButtonStyle(
                          padding: MaterialStateProperty.all(
                              const EdgeInsets.all(15))),
                      child: const Text("INGRESAR")),
                  const SizedBox(
                    height: 30.0,
                  ),
                  Center(
                    child: RichText(
                      text: TextSpan(
                        children: [
                          const TextSpan(
                            text: 'No tienes una cuenta? ',
                            style: TextStyle(color: Colors.black),
                          ),
                          TextSpan(
                            text: 'REGISTRATE',
                            style: const TextStyle(
                                color: Color(0xFF212a72),
                                fontWeight: FontWeight.bold),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Navigator.pushNamed(context, Routes.SINGUP);
                              },
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
