import 'package:flutter/cupertino.dart';
import 'package:todolist/app/domain/model/tasks_model.dart';
import 'package:todolist/app/domain/repository/local_repository_interface.dart';
import 'package:todolist/app/domain/repository/tasks_repository_interface.dart';
import 'package:todolist/app/presentation/widgets/progress_dialog.dart';

class HomeController with ChangeNotifier {

  final LocalRepositoryInterface localRepositoryInterface;
  final TasksRepositoryInterface tasksRepositoryInterface;

  HomeController({required this.localRepositoryInterface, required this.tasksRepositoryInterface});

  List<TasksModel> listDataTasks = [];

  bool _loadingTasks = false;

  bool isLoadingTasks() => _loadingTasks;

  Future listTasks() async {
    _loadingTasks = true;
    listDataTasks = await tasksRepositoryInterface.listTasks();
    _loadingTasks = false;
    notifyListeners();
  }

  Future changeStatus(BuildContext context, String taskId, String status) async {
    ProgressDialog.show(context);
    await tasksRepositoryInterface.changeStatus(taskId, status);
    await listTasks();
    Navigator.of(context).pop();
  }

  Future deleteTask(BuildContext context, String taskId) async {
    ProgressDialog.show(context);
    await tasksRepositoryInterface.deleteTask(taskId);
    await listTasks();
    Navigator.of(context).pop();
  }

}