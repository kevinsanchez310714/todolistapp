import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist/app/domain/model/tasks_model.dart';
import 'package:todolist/app/domain/repository/local_repository_interface.dart';
import 'package:todolist/app/domain/repository/tasks_repository_interface.dart';
import 'package:todolist/app/presentation/global_controller/session_controller.dart';
import 'package:todolist/app/presentation/routes/routes.dart';
import 'package:todolist/app/presentation/screens/home/home_controller.dart';

class HomeScreen extends StatefulWidget{
  const HomeScreen._();

  static Widget init(BuildContext context){
    return ChangeNotifierProvider(
      create: (_) => HomeController(
        localRepositoryInterface: context.read<LocalRepositoryInterface>(),
        tasksRepositoryInterface: context.read<TasksRepositoryInterface>()
      )..listTasks(),
      builder: (_, __) => const HomeScreen._(),
    );
  }

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    final sessionController = Provider.of<SessionController>(context);
    final controller = Provider.of<HomeController>(context);
    return Scaffold(
      backgroundColor: const Color(0XFFF7F7F7),
      appBar: AppBar(
        title: const Text("ToDO List App"),
        actions: [
          IconButton(
            onPressed: () async {
              await sessionController.authRepositoryInterface.singOut();
              Navigator.of(context).pushNamedAndRemoveUntil(Routes.LOGIN, (route) => false);
            },
            icon: const Icon(Icons.exit_to_app)
          )
        ],
      ),
      body: SafeArea(
        child: DefaultTabController(
            length: 2,
            initialIndex: 0,
            child: CustomScrollView(
              slivers: [
                SliverFillRemaining(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 10.0,),
                      SizedBox(
                        height: 45,
                        child: TabBar(
                          labelColor: Color(0XFF006B5B),
                          unselectedLabelColor: Colors.grey,
                          indicatorColor: Color(0XFF006B5B),
                          padding: const EdgeInsets.only(left: 20.0, top: 5.0, bottom: 5.0, right: 20.0),
                          labelPadding: const EdgeInsets.all(0),
                          indicatorPadding: EdgeInsets.zero,
                          indicator: BoxDecoration(
                            color: Color(0XFF006B5B).withOpacity(0.5),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          labelStyle: Theme.of(context).textTheme.headline1!.copyWith(
                            fontSize: 12.0,
                            fontWeight: FontWeight.bold,
                          ),
                          tabs: const [
                            Tab(child: Text("Pendientes", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0),),),
                            Tab(child: Text("Completadas", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0),),),
                          ],
                        ),
                      ),
                      Expanded(
                        child: TabBarView(
                          children: [
                            Consumer<HomeController>(
                              builder: (context, value, child) {
                                if(value.isLoadingTasks()) {
                                  return const Padding(
                                    padding: EdgeInsets.only(top: 300.0),
                                    child: Center(child: CupertinoActivityIndicator(radius: 20.0, color: Color(0XFF006B5B),)),
                                  );
                                }
                                List<TasksModel> pendingTasks = value.listDataTasks.where((task) => task.status == 'Pendiente').toList();
                                if(pendingTasks.isEmpty){
                                  return const Center(child: Text("No hay tareas pendientes", style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),));
                                }
                                return ListView.builder(
                                  padding: const EdgeInsets.all(15.0),
                                  itemCount: pendingTasks.length,
                                  shrinkWrap: true,
                                  itemBuilder: (context, index){
                                    final task = pendingTasks[index];
                                    return card(
                                      context: context,
                                      taskid: task.id,
                                      title: task.title,
                                      description: task.description,
                                      fecha: task.date,
                                      status: task.status
                                    );
                                  },
                                );
                              },
                            ),
                            Consumer<HomeController>(
                              builder: (context, value, child) {
                                if(value.isLoadingTasks()) {
                                  return const Padding(
                                    padding: EdgeInsets.only(top: 300.0),
                                    child: Center(child: CupertinoActivityIndicator(radius: 20.0, color: Color(0XFF006B5B),)),
                                  );
                                }
                                List<TasksModel> completedTasks = value.listDataTasks.where((task) => task.status == 'Completado').toList();
                                if(completedTasks.isEmpty){
                                  return const Center(child: Text("No hay tareas completadas", style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),));
                                }
                                return ListView.builder(
                                  padding: const EdgeInsets.all(15.0),
                                  itemCount: completedTasks.length,
                                  shrinkWrap: true,
                                  itemBuilder: (context, index){
                                    final task = completedTasks[index];
                                    return card(
                                        context: context,
                                        taskid: task.id,
                                        title: task.title,
                                        description: task.description,
                                        fecha: task.date,
                                        status: task.status
                                    );
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
        ),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(15.0),
        child: FloatingActionButton(
          onPressed: () {
            Navigator.of(context).pushNamed(Routes.TASKS);
          },
          tooltip: 'Nueva tarea',
          backgroundColor: const Color(0xFF006B5B),
          child: const Icon(Icons.add),
        ),
      ),
    );
  }

  Widget card({required BuildContext context, required String taskid, required String title, required String description, required String fecha, required String status }){
    final controller = Provider.of<HomeController>(context);
    return Center(
      child: Row(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                backgroundColor: status == 'Pendiente' ? Colors.grey : const Color(0xFF006B5B),
                child: IconButton(
                  icon: const Icon(Icons.check),
                  color: Colors.white,
                  onPressed: () async {
                    if(status == 'Pendiente'){
                      await controller.changeStatus(context, taskid, 'Completado');
                    }else{
                      await controller.changeStatus(context, taskid, 'Pendiente');
                    }
                  },
                ),
              ),
              const SizedBox(height: 10.0,),
              CircleAvatar(
                backgroundColor: Colors.red,
                child: IconButton(
                  icon: const Icon(Icons.delete_outline),
                  color: Colors.white,
                  onPressed: () async {
                    await controller.deleteTask(context, taskid);
                  },
                ),
              ),
              const SizedBox(height: 15.0,),
            ],
          ),
          const SizedBox(width: 10.0,),
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(bottom: 20.0),
              padding: const EdgeInsets.only(left: 15.0, top: 15.0, bottom: 15.0, right: 15.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12.0),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(title??'', style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                      decoration: status == 'Completado' ? TextDecoration.lineThrough : TextDecoration.none,
                      color: status == 'Completado' ? Colors.grey : Colors.black,
                  ),),
                  const SizedBox(height: 10.0,),
                  Text(
                    description??'',
                    style: TextStyle(
                      decoration: status == 'Completado' ? TextDecoration.lineThrough : TextDecoration.none,
                      color: status == 'Completado' ? Colors.grey : Colors.black,
                    ),
                  ),
                  const SizedBox(height: 5.0,),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        fecha??'',
                        style: TextStyle(
                          decoration: status == 'Completado' ? TextDecoration.lineThrough : TextDecoration.none,
                          color: status == 'Completado' ? Colors.grey : Colors.grey,
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: IconButton(
                          padding: EdgeInsets.zero,
                          constraints: const BoxConstraints(),
                          iconSize: 20,
                          onPressed: () {},
                          icon: const Icon(Icons.translate, color: Color(0xFF212a72),)
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}