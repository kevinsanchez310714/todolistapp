
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:todolist/app/domain/repository/local_repository_interface.dart';
import 'package:todolist/app/my_app_controller.dart';
import 'package:todolist/app/presentation/global_controller/session_controller.dart';
import 'package:todolist/app/presentation/routes/routes.dart';

class SplashController with ChangeNotifier {
  final SessionController _sessionController;
  final LocalRepositoryInterface localRepositoryInterface;
  final BuildContext context;

  String? _routeName;
  String? get routeName => _routeName;

  SplashController(this._sessionController, {required this.context, required this.localRepositoryInterface}){
    _init();
  }


  void _init() async {
    var authRepository = Provider.of<MyAppController>(context);
    final user = await authRepository.authRepositoryInterface.user;

    if(user != null) {
      _routeName = Routes.HOME;
      _sessionController.setUser(user);
    }else{
      _routeName = Routes.LOGIN;
    }

    notifyListeners();
  }

}