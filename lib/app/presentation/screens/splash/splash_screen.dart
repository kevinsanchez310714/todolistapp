import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist/app/domain/repository/local_repository_interface.dart';
import 'package:todolist/app/my_app_controller.dart';
import 'package:todolist/app/presentation/global_controller/session_controller.dart';
import 'package:todolist/app/presentation/screens/splash/splash_controller.dart';

class SplashScreen extends StatefulWidget {

  SplashScreen._();

  static Widget init(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => SplashController(
        context.read<SessionController>(),
        context: context,
        localRepositoryInterface: context.read<LocalRepositoryInterface>()
      ),
      builder: (_, __) => SplashScreen._(),
    );
  }

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  Widget build(BuildContext context) {
    final controller = Provider.of<SplashController>(context);
    return Consumer<SplashController>(
      builder: (cxt, value, child){
        final routeName = controller.routeName;
        if(routeName != null){
          Future.delayed(Duration.zero, () {
            Navigator.pushReplacementNamed(context, routeName);
          });
        }
        return const Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }
}