import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todolist/app/domain/repository/local_repository_interface.dart';
import 'package:todolist/app/domain/repository/sing_up_repository_interface.dart';
import 'package:todolist/app/domain/request/sing_up_data_request.dart';
import 'package:todolist/app/domain/response/sing_up_response.dart';
import 'package:todolist/app/presentation/global_controller/session_controller.dart';
import 'package:todolist/app/presentation/routes/routes.dart';
import 'package:todolist/app/presentation/widgets/dialogs.dart';
import 'package:todolist/app/presentation/widgets/progress_dialog.dart';

class SingUpController with ChangeNotifier {
  final SessionController _sessionController;
  final LocalRepositoryInterface localRepositoryInterface;
  final SingUpRepositoryInterface singUpRepositoryInterface;

  SingUpController(this._sessionController, {
    required this.localRepositoryInterface,
    required this.singUpRepositoryInterface
  });

  final GlobalKey<FormState> formKey = GlobalKey();

  TextEditingController nombre = TextEditingController();
  TextEditingController apellido = TextEditingController();
  TextEditingController correo = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController confirmPassword = TextEditingController();

  Future<void> sendRegisterForm(BuildContext context) async {
    final isValidForm = formKey.currentState!.validate();

    if(isValidForm){
      ProgressDialog.show(context);
      final response = await submit();
      Navigator.pop(context);
      if(response.error != null){
        late String content;
        switch(response.error){
          case SingUpError.emailAlreadyInUse:
            content = "No puedes usar este correo";
            break;
          case SingUpError.networkRequestFailed:
            content = "No hay acceso a la red";
            break;
          case SingUpError.weakPassword:
            content = "La contraseña es insegura";
            break;
          case SingUpError.unknown:
            content = "Ha ocurrido un error";
            break;
          default:
            content = "Ha ocurrido un error";
            break;
        }
        Dialogs.alert(context, title: 'ERROR', content: content);
      }else{
        Navigator.of(context).pushNamedAndRemoveUntil(Routes.HOME, (route) => false);
      }
    }else{
      Dialogs.alert(context, title: 'ERROR', content: 'Verifique que todos los campos sean validos' );
    }
  }

  Future<SingUpResponse> submit() async {
    final response = await singUpRepositoryInterface.register(SingUpDataRequest(
      name: nombre.text,
      lastname: apellido.text,
      email: correo.text,
      password: password.text
    ));

    if(response.error == null){
      _sessionController.setUser(response.user!);
    }

    return response;
  }

}