import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist/app/domain/repository/local_repository_interface.dart';
import 'package:todolist/app/domain/repository/sing_up_repository_interface.dart';
import 'package:todolist/app/presentation/global_controller/session_controller.dart';
import 'package:todolist/app/presentation/screens/sing_up/sing_up_controller.dart';
import 'package:todolist/app/presentation/widgets/custom_input_field.dart';
import 'package:todolist/app/utils/input_validator.dart';

class SingUpScreen extends StatefulWidget {
  const SingUpScreen._();

  static Widget init(BuildContext context){
    return ChangeNotifierProvider(
      create: (_) => SingUpController(
        context.read<SessionController>(),
        localRepositoryInterface: context.read<LocalRepositoryInterface>(),
        singUpRepositoryInterface: context.read<SingUpRepositoryInterface>()
      ),
      builder: (_, __) => const SingUpScreen._(),
    );
  }

  @override
  State<SingUpScreen> createState() => _SingUpScreenState();
}

class _SingUpScreenState extends State<SingUpScreen> {

  @override
  Widget build(BuildContext context) {
    final controller = Provider.of<SingUpController>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Crear cuenta"),
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.transparent,
          child: Form(
            key: controller.formKey,
            child: ListView(
              padding: const EdgeInsets.all(15.0),
              children: [
                Image.asset("assets/logo.png", height: 180,),
                const SizedBox(
                  height: 50,
                ),
                CustomInputField(
                  controller: controller.nombre,
                  onChanged: (text) {},
                  label: "Nombre",
                  validator: (text){
                    return isValidName(text!) ? null : "Nombre invalido";
                  },
                ),
                const SizedBox(height: 15,),
                CustomInputField(
                  controller: controller.apellido,
                  onChanged: (text) {},
                  label: "Apellido",
                  validator: (text){
                    return isValidName(text!) ? null : "Apellido invalido";
                  },
                ),
                const SizedBox(height: 15,),
                CustomInputField(
                  controller: controller.correo,
                  onChanged: (text) {},
                  label: "Correo",
                  inputType: TextInputType.emailAddress,
                  validator: (text){
                    return isValidEmail(text!) ? null : "Correo invalido";
                  },
                ),
                const SizedBox(height: 15,),
                CustomInputField(
                  controller: controller.password,
                  onChanged: (text) {},
                  label: "Contraseña",
                  isPassword: true,
                  validator: (text) {
                    if(text!.trim().length >= 6){
                      return null;
                    }
                    return "Contraseña invalida";
                  },
                ),
                const SizedBox(height: 15,),
                Consumer<SingUpController>(
                  builder: (cnt, value, child){
                    return CustomInputField(
                      controller: controller.confirmPassword,
                      onChanged: (text) {},
                      label: "Confirme contraseña",
                      isPassword: true,
                      validator: (text) {
                        if(value.password.text == value.confirmPassword.text){
                          return null;
                        }
                        return "Contraseña invalida";
                      },
                    );
                  },
                ),
                const SizedBox(height: 30,),
                ElevatedButton(
                  style: ButtonStyle(
                    padding: MaterialStateProperty.all(const EdgeInsets.all(15.5))
                  ),
                  child: const Text("REGISTRARME"),
                  onPressed: () => controller.sendRegisterForm(context),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}