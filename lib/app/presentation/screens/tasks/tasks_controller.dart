import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:todolist/app/domain/model/tasks_model.dart';
import 'package:todolist/app/domain/repository/local_repository_interface.dart';
import 'package:todolist/app/domain/repository/tasks_repository_interface.dart';
import 'package:todolist/app/domain/request/tasks_data_request.dart';
import 'package:todolist/app/presentation/routes/routes.dart';
import 'package:todolist/app/presentation/widgets/progress_dialog.dart';

class TasksController with ChangeNotifier {

  final LocalRepositoryInterface localRepositoryInterface;
  final TasksRepositoryInterface tasksRepositoryInterface;

  final GlobalKey<FormState>  formKey = GlobalKey();

  TextEditingController title = TextEditingController();
  TextEditingController description = TextEditingController();
  TextEditingController date = TextEditingController();

  List<String> itemsStatus = ["Pendiente", "Completado"];

  String currentStatus = "";

  TasksController({required this.localRepositoryInterface, required this.tasksRepositoryInterface}){
    currentStatus = itemsStatus[0];
    date.text = DateFormat("yyyy-MM-dd").format(DateTime.now());
  }

  Future<void> sendFormTask(BuildContext context) async {
    if(formKey.currentState!.validate()){
      ProgressDialog.show(context);
      await submit();
      Navigator.of(context).pop();
      Navigator.of(context).pushNamedAndRemoveUntil(Routes.HOME, (route) => false);
    }
  }

  Future submit() async {
    await tasksRepositoryInterface.createTask(TaskDataRequest(
        title: title.text,
        description: description.text,
        status: currentStatus,
        date: date.text
    ));
  }

}