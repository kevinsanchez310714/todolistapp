import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todolist/app/domain/repository/local_repository_interface.dart';
import 'package:todolist/app/domain/repository/tasks_repository_interface.dart';
import 'package:todolist/app/presentation/screens/tasks/tasks_controller.dart';
import 'package:todolist/app/presentation/widgets/custom_input_field.dart';

class TasksScreen extends StatefulWidget {
  TasksScreen._();

  static Widget init(BuildContext context){
    return ChangeNotifierProvider(
      create: (_) => TasksController(
        localRepositoryInterface: context.read<LocalRepositoryInterface>(),
        tasksRepositoryInterface: context.read<TasksRepositoryInterface>()
      ),
      builder: (_, __) => TasksScreen._(),
    );
  }

  @override
  State<TasksScreen> createState() => _TasksScreenState();
}

class _TasksScreenState extends State<TasksScreen> {

  void callDatePicker() async {
    final controller = Provider.of<TasksController>(context, listen: false);
    var selectDate = await getDatePickerWidget();
    setState(() {
      controller.date.text = DateFormat("yyyy-MM-dd").format(selectDate!);
    });
  }

  Future<DateTime?> getDatePickerWidget() {
    final controller = Provider.of<TasksController>(context, listen: false);
    return showDatePicker(
      context: context,
      initialDate: DateTime.parse(controller.date.text),
      firstDate: DateTime(2024),
      lastDate: DateTime(2030),
      builder: (context, child) {
        return Theme(
          data: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: const Color(0xFF16af97)),
          ),
          child: child!
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    final controller = Provider.of<TasksController>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Agregar tarea"),
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.transparent,
          child: Form(
            key: controller.formKey,
            child: ListView(
              padding: const EdgeInsets.all(10.0),
              children: [
                const SizedBox(height: 15.0,),
                CustomInputField(
                  controller: controller.title,
                  label: "Titulo",
                  validator: (text){
                    return text!.isNotEmpty ? null : "Escriba un titulo";
                  },
                ),
                const SizedBox(height: 15.0,),
                CustomInputField(
                  controller: controller.description,
                  label: "Descripción",
                  isLargeText: true,
                  validator: (text){
                    return text!.isNotEmpty ? null : "Escriba una descripción";
                  },
                ),
                const SizedBox(height: 15.0,),
                InputDecorator(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(4.0))),
                    contentPadding: EdgeInsets.only(top: 20.0, bottom: 20.0, left: 15.0, right: 15.0),
                  ),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton<String>(
                      value: controller.currentStatus,
                      isDense: true,
                      isExpanded: true,
                      items: controller.itemsStatus
                          .map<DropdownMenuItem<String>>(
                            (e) => DropdownMenuItem(
                          value: e,
                          child: Text(e),
                        ),
                      ).toList(),
                      onChanged: (value) {
                        setState(() {
                          if (value != null) controller.currentStatus = value;
                        });
                      },
                    ),
                  ),
                ),
                const SizedBox(height: 15.0,),
                CustomInputField(
                  controller: controller.date,
                  label: "Fecha",
                  onTap: () => callDatePicker(),
                  inputType: TextInputType.none,
                  icon: Icons.date_range,
                ),
                const SizedBox(height: 50.0,),
                ElevatedButton(
                  onPressed: () async {
                    await controller.sendFormTask(context);
                  },
                  style: ButtonStyle(
                      padding: MaterialStateProperty.all(const EdgeInsets.all(15))
                  ),
                  child: const Text("AGREGAR")
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

}