import 'package:flutter/cupertino.dart';
import 'package:todolist/app/domain/repository/auth_repository_interface.dart';
import 'package:todolist/app/domain/repository/local_repository_interface.dart';

class MyAppController with ChangeNotifier{
  final LocalRepositoryInterface localRepositoryInterface;
  final AuthRepositoryInterface authRepositoryInterface;

  MyAppController({ required this.localRepositoryInterface, required this.authRepositoryInterface});

}