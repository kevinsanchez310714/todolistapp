import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:todolist/app/my_app.dart';
import 'package:todolist/firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: const FirebaseOptions(
      apiKey: "AIzaSyCTuf3prfFCUIUN-cP4OYpuodjnvLB3XGo",
      appId: "1:311358657812:web:48068ad9ae7871eff99e01",
      messagingSenderId: "311358657812",
      projectId: "todolistapp-efec9"
    )
  );
  runApp(MyApp());
}
